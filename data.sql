-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 05, 2023 at 04:24 PM
-- Server version: 10.4.27-MariaDB
-- PHP Version: 8.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `data`
--

-- --------------------------------------------------------

--
-- Table structure for table `books`
--

CREATE TABLE `books` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_buku` text NOT NULL,
  `kategori` text NOT NULL,
  `nama_buku` text NOT NULL,
  `harga` text NOT NULL,
  `stok` text NOT NULL,
  `penerbit` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `books`
--

INSERT INTO `books` (`id`, `id_buku`, `kategori`, `nama_buku`, `harga`, `stok`, `penerbit`, `created_at`, `updated_at`) VALUES
(1, 'K1001', 'Keilmuan', 'Analisis & Perancangan Sistem Informasi', '50.000', '60', 'Penerbit Informatika', '2023-06-05 10:23:08', '2023-06-05 06:14:20'),
(2, 'K1002', 'Keilmuan', 'Artificial Intelegency', '45000', '60', 'Penerbit Informatika', NULL, NULL),
(6, 'K2003', 'Keilmuan', 'Autocad 3 Dimensi', '40000', '25', 'Penerbit Informatika', '2023-06-05 05:19:04', '2023-06-05 05:19:04'),
(7, 'B1001', 'Bisnis', 'Bisnis Online', '75000', '9', 'Penerbit Informatika', '2023-06-05 05:19:26', '2023-06-05 06:07:56'),
(8, 'K3004', 'Keilmuan', 'Cloud Computing Technology', '85000', '15', 'Penerbit Informatika', '2023-06-05 05:19:51', '2023-06-05 05:19:51'),
(9, 'B1002', 'Bisnis', 'Etika Bisnis dan Tanggung Jawab Sosial', '67500', '10', 'Penerbit Informatika', '2023-06-05 05:20:20', '2023-06-05 05:20:44'),
(10, 'N1001', 'Novel', 'Cahaya Di Penjuru Hati', '68000', '10', 'Andi Offset', '2023-06-05 05:21:03', '2023-06-05 05:21:03'),
(11, 'N1002', 'Novel', 'Aku Ingin Cerita', '48000', '12', 'Danendra', '2023-06-05 05:21:19', '2023-06-05 05:21:19');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `books`
--
ALTER TABLE `books`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `books`
--
ALTER TABLE `books`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
