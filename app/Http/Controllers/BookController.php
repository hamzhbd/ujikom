<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Book;


class BookController extends Controller
{
    public function index()
    {
        $books = Book::all();
        return view('books.index', compact('books'));
    }
    
    public function search(Request $request)
    {
        $search = $request->input('search');
        $books = Book::where('nama_buku', 'like', '%' . $search . '%')->get();
        return view('books.index', compact('books', 'search'));
    }
    
    public function admin()
    {
        $books = Book::all();
        return view('books.admin', compact('books'));
    }
    
    public function pengadaan()
    {
        // $books = Book::orderBy('stok', 'desc')->get();
        $books = Book::orderByRaw("CAST(SUBSTRING_INDEX(stok, ' ', 1) AS UNSIGNED) ASC")->get();
        return view('books.pengadaan', compact('books'));
    }

    public function create()
    {
        return view('books.create');
    }
    public function store(Request $request)
    {
        $book = new Book();
        $book->id_buku = $request->input('id_buku');
        $book->kategori = $request->input('kategori');
        $book->nama_buku = $request->input('nama_buku');
        $book->harga = $request->input('harga');
        $book->stok = $request->input('stok');
        $book->penerbit = $request->input('penerbit');
        $book->save();
    
        return redirect()->route('admin');
    }
    
    public function edit($id)
    {
        $book = Book::find($id);
        return view('books.edit', compact('book'));
    }
    
    public function update(Request $request, $id)
    {
        $book = Book::find($id);
        $book->id_buku = $request->input('id_buku');
        $book->kategori = $request->input('kategori');
        $book->nama_buku = $request->input('nama_buku');
        $book->harga = $request->input('harga');
        $book->stok = $request->input('stok');
        $book->penerbit = $request->input('penerbit');
        $book->save();
    
        return redirect()->route('admin');
    }
    
    public function destroy($id)
    {
        $book = Book::find($id);
        $book->delete();
    
        return redirect()->route('admin');
    }
}
