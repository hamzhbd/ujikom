<!-- resources/views/books/edit.blade.php -->

@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>Edit Book</h1>

        <form action="{{ route('books.update', $book->id) }}" method="POST">
            @csrf
            @method('PATCH')
            <div class="form-group">
                <label for="id_buku">ID Buku:</label>
                <input type="text" name="id_buku" id="id_buku" class="form-control" value="{{ $book->id_buku }}">
            </div>

            <div class="form-group">
                <label for="kategori">Kategori:</label>
                <input type="text" name="kategori" id="kategori" class="form-control" value="{{ $book->kategori }}">
            </div>

            <div class="form-group">
                <label for="nama_buku">Nama Buku:</label>
                <input type="text" name="nama_buku" id="nama_buku" class="form-control" value="{{ $book->nama_buku }}">
            </div>

            <div class="form-group">
                <label for="harga">Harga:</label>
                <input type="text" name="harga" id="harga" class="form-control" value="{{ $book->harga }}">
            </div>

            <div class="form-group">
                <label for="stok">Stok:</label>
                <input type="text" name="stok" id="stok" class="form-control" value="{{ $book->stok }}">
            </div>

            <div class="form-group">
                <label for="penerbit">Penerbit:</label>
                <input type="text" name="penerbit" id="penerbit" class="form-control" value="{{ $book->penerbit }}">
            </div>

            <button type="submit" class="btn btn-primary">Update</button>
        </form>
    </div>
@endsection
