<!-- pengadaan.blade.php -->

@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>Laporan Kebutuhan Buku</h1>

        <table class="table">
            <thead>
                <tr>
                    <th>Judul Buku</th>
                    <th>Nama Penerbit</th>
                    <th>Sisa Buku</th>
                </tr>
            </thead>
            <tbody>
                @foreach($books as $book)
                    <tr>
                        <td>{{ $book->nama_buku }}</td>
                        <td>{{ $book->penerbit }}</td>
                        <td>{{ $book->stok}}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection
