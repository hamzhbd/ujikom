@extends('layouts.app')

@section('content')
<div class="container">

    <h1>Daftar Buku</h1>
    <form method="GET" action="{{ route('search') }}">
        <input type="text" name="search" placeholder="Nama Buku" value="{{ $search ?? '' }}">
        <button type="submit">Cari</button>
    </form>
    <table class="table">

    <thead>
        <tr>
            <th>ID Buku</th>
            <th>Kategori</th>
            <th>Nama Buku</th>
            <th>Harga</th>
            <th>Stok</th>
            <th>Penerbit</th>
        </tr>
    </thead>
        @foreach ($books as $book)
        <tbody>
            <tr>
                <td>{{ $book->id_buku }}</td>
                <td>{{ $book->kategori }}</td>
                <td>{{ $book->nama_buku }}</td>
                <td>{{ $book->harga }}</td>
                <td>{{ $book->stok }}</td>
                <td>{{ $book->penerbit }}</td>
            </tr>
        </tbody>
        @endforeach
    </table>
    <div class="container">
        <h1>Tabel Penerbit</h1>

        <table class="table">
            <thead>
                <tr>
                    <th>ID Penerbit</th>
                    <th>Nama</th>
                    <th>Alamat</th>
                    <th>Kota</th>
                    <th>Telepon</th>
                </tr>
            </thead>
            <tbody>
                    <tr>
                        <td>SP01</td>
                        <td>Penerbit Informatika</td>
                        <td>Jl. Buah Batu No.121</td>
                        <td>Bandung</td>
                        <td>0813-2220-1946</td>
                    </tr>
                    <tr>
                        <td>SP02</td>
                        <td>Andi Offset</td>
                        <td>Jl. Suryalaya IX No.3</td>
                        <td>Bandung</td>
                        <td>0878-3903-0688</td>
                    </tr>
                    <tr>
                        <td>SP03</td>
                        <td>Danendra</td>
                        <td>Jl. Moch Toha 44</td>
                        <td>Bandung</td>
                        <td>022-5201215</td>
                    </tr>
            </tbody>
        </table>
    </div>
@endsection
