@extends('layouts.app')

@section('content')
    <h1>Pengolahan Data Buku</h1>
    <a href="{{ route('books.create') }}" class="btn btn-primary">Tambah Buku</a>
    <table class="table">
    <tr>
            <th>ID Buku</th>
            <th>Kategori</th>
            <th>Nama Buku</th>
            <th>Harga</th>
            <th>Stok</th>
            <th>Penerbit</th>
            <th>Aksi</th>
        </tr>
        @foreach ($books as $book)
            <tr>
                <td>{{ $book->id_buku }}</td>
                <td>{{ $book->kategori }}</td>
                <td>{{ $book->nama_buku }}</td>
                <td>{{ $book->harga }}</td>
                <td>{{ $book->stok }}</td>
                <td>{{ $book->penerbit }}</td>
                <td>
                    <a href="{{ route('books.edit', $book->id) }}" class="btn btn-primary">Edit</a>
                    <form action="{{ route('books.destroy', $book->id) }}" method="POST" style="display: inline;">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-danger">Hapus</button>
                    </form>
                </td>
            </tr>
        @endforeach
<thead>
    <table>
       
    </table>
@endsection
